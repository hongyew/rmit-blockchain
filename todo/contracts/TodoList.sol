pragma solidity 0.4.24;
pragma experimental ABIEncoderV2;

contract TodoList {
    struct Task {
        string description;
        bool   completed;
    }
    struct Tasks {
        Task[] tasks;
    }

    // Creator of the contract. Public getter, accessible by owner() metjhod.
    address public owner;

    // I can't declare public -- mapping(address => Tasks) public usersTasks;
    // "internal or recursive type is not allowed for public state variable"
    mapping(address => Tasks) usersTasks;

    // Keep track of a list of users (addresses). Want it to be a unique list
    // Getter makes it possible to access the array item only by .users(index).
    address[] public users;

    // Keep a copy of the original creator
    constructor () public {
        owner = msg.sender;
    }

    function get() public view returns (Task[] retVal) {
        return usersTasks[msg.sender].tasks;
    }

    function add(string description) public {
        registerUserIfNecessary(msg.sender);
        usersTasks[msg.sender].tasks.push(Task({description: description, completed: false}));
    }

    function registerUserIfNecessary(address currentUser) internal {
        // Check to see if the user address already exists
        bool found = false;
        for (uint i = 0; i < users.length; i++) {
            if (users[i] == currentUser) {
                found = true;
                break;
            }
        }
        if (!found) users.push(currentUser);
    }

    function tasks() public view returns (Tasks) {
        return usersTasks[msg.sender];
    }

    function task(uint i) public view returns (Task) {
        return tasks().tasks[i];
    }

    function count() public view returns (uint) {
        return tasks().tasks.length;
    }

    function done(uint i) public {
        usersTasks[msg.sender].tasks[i].completed = true;
    }

    function undo(uint i) public {
        usersTasks[msg.sender].tasks[i].completed = false;
    }

    function numberOfUsers() public view returns (uint) {
        return users.length;
    }

    function clear() public {
        uint usersCount = users.length;
        delete usersTasks[msg.sender];
        for (uint i = 0; i < usersCount; i++) {
            if (users[i] == msg.sender) delete users[i];
        }
    }

    function clearAll() public /*ownerOnly*/ {
        uint usersCount = users.length;
        for (uint i = 0; i < usersCount; i++) {
            delete usersTasks[users[i]];
            delete users[i];
        }
    }

    modifier ownerOnly {
        require(msg.sender == owner, "Only owner can call this function.");
        _;
    }

    // Attempted to return an array, but won't compile.
    // The warning asked me to add 'memory'. Not sure what it is, but push() won't work after that.

    // function completedTasks() public returns (Task[]) {
    //     Task[] memory _completed;
    //     for (uint i = 0; i<tasks.length; i++) {
    //         if (tasks[i].completed) _completed.push(tasks[i]);
    //     }
    //     return _completed;
    // }
}