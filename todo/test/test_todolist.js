
const TodoList = require('Embark/contracts/TodoList');

// For documentation please see https://embark.status.im/docs/contracts_testing.html
config({
    contracts: {
        "TodoList": {}
    }
}, (_err) => {

});

// Tests - Note that the tests are stateful. SO it must be executed
// in sequence.
contract("TodoList", () => {

    // Add item to task list
    it("should add to task list", async () => {
        await TodoList.methods.add("task 1").send();
        let result = await TodoList.methods.get().call();
        console.log(result);
        
        assert.strictEqual(result.length, 1, 'Expects 1 record');
        assert.strictEqual(result[0].description, 'task 1'); 
        assert.strictEqual(result[0].completed, false); 
    });

    // Update a task
    it("complete a task", async () => {
        await TodoList.methods.done(0).send();
        let result = await TodoList.methods.get().call();
        console.log(result);        
        assert.strictEqual(result[0].completed, true, 'The task should be completed'); 
    });

    it("count number of tasks", async () => {
        let result = await TodoList.methods.count().call(); // CAUTION: this returns a string, not int!!
        console.log(typeof result);  // Type not preserve from eth -> js? result is 'string'. expects unsigned int.      
        assert.equal(result, 1, 'Incorrect number of tasks'); 
    });

    it("contract owner is recorded", async () => {
        let result = await TodoList.methods.owner().call();
        console.log('Owner: ' + result);
        assert.ok(result != null, 'Onwer address should be not null'); // Expects no record
    });


    it("There is one users registered", async () => {
        let result = parseInt(await TodoList.methods.numberOfUsers().call());  // Why does it return 'string' not 'int'.
        console.log('Length of users array (numberOfUsers): ' + result);
        console.log(await TodoList.methods.users(0).call()); // display the first user address
        assert.strictEqual(result, 1, 'Incorrect number of user registered'); // Expects no record
    });

    // Add 2nd item to task list
    it("should add a second task", async () => {
        await TodoList.methods.add("task 2").send();
        let result = await TodoList.methods.get().call();
        console.log(result);
        
        assert.strictEqual(result.length, 2, 'Expects 2 records');
        assert.strictEqual(result[1].description, 'task 2'); 
        assert.strictEqual(result[1].completed, false); 
    });

    it("There is still one users registered", async () => {
        let result = parseInt(await TodoList.methods.numberOfUsers().call());  // Why does it return 'string' not 'int'.
        console.log('Length of users array (numberOfUsers): ' + result);
        console.log(await TodoList.methods.users(0).call()); // display the first user address
        assert.strictEqual(result, 1, 'Incorrect number of user registered'); // Expects no record
    });

       // LAST
    it("Clears", async () => {
        await TodoList.methods.clear().send();

        // Check there should be no tasks
        let result = await TodoList.methods.get().call();
        console.log(result);
        assert.strictEqual(result.length, 0, 'Expects no record');
    });

    // LAST
    it("Clears All", async () => {
        await TodoList.methods.add("task again").send();
        await TodoList.methods.clearAll().send();

        // Check there should be no tasks
        let result = await TodoList.methods.get().call();
        console.log(result);
        assert.strictEqual(result.length, 0, 'Expects no record');
    });

});