
const Etherpost = require('Embark/contracts/Etherpost');

// For documentation please see https://embark.status.im/docs/contracts_testing.html
config({
    contracts: {
        "Etherpost": {}
    }
}, (_err) => {

});

// Tests - Note that the tests are stateful. SO it must be executed
// in sequence.
contract("Etherpost Comments", () => {

    // Test claps. Assuming that the blockchain already has the image hashes from
    // earlier tests.
    describe("Assuming there is an image hash", async () => {
        const imageHash = web3.utils.randomHex(32);
        const commentHash1 = web3.utils.randomHex(32);
        const commentHash2 = web3.utils.randomHex(32);

        it("should be no comment on the image at the beginning", async () => {
            const result = await Etherpost.methods.getComments(imageHash).call();
            assert.strictEqual(result.length, 0);
        });

        it("should allow me to add a comment hash against an image hash", async () => {
            await Etherpost.methods.comment(imageHash, commentHash1).send();
            const result = await Etherpost.methods.getComments(imageHash).call();
            assert.strictEqual(result.length, 1);
            assert.strictEqual(result[0], commentHash1);
        });

        // Continues from the previous test. There should already be 1 comment against this image hash before the test.
        it("should allow me to add multiple comment hashes against an image hash", async () => {
            await Etherpost.methods.comment(imageHash, commentHash2).send();
            const result = await Etherpost.methods.getComments(imageHash).call();
            assert.strictEqual(result.length, 2);
            assert.strictEqual(result[0], commentHash1);  // from previousl test already in the blockchain
            assert.strictEqual(result[1], commentHash2);
        });

        it("should raise LogComment event", async () => {
            Etherpost.events.LogComment((err, event) => {
                assert.ifError(err)  // no error
                //console.log(event)
                assert.strictEqual(event.event, 'LogComment');
                assert.strictEqual(event.returnValues.imageHash, imageHash);
                assert.strictEqual(event.returnValues.commentHash, commentHash2);
            });
            const accounts = await web3.eth.getAccounts();
            await Etherpost.methods.comment(imageHash, commentHash2).send();
        });
    });
});