
const Etherpost = require('Embark/contracts/Etherpost');

// For documentation please see https://embark.status.im/docs/contracts_testing.html
config({
    contracts: {
        "Etherpost": {}
    }
}, (_err) => {

});

// Tests - Note that the tests are stateful. SO it must be executed
// in sequence.
contract("Etherpost", () => {

    // Test Picture IPFS uploads and fetch
    describe("Given I have post contract", async () => {
        const hash1 = web3.utils.randomHex(32); 
        const hash2 = web3.utils.randomHex(32); 
        const hash3 = web3.utils.randomHex(32); 
                
        it("should be stored against the original sender", async () => {
            const accounts = await web3.eth.getAccounts();
            await Etherpost.methods.upload(hash1).send({ from: accounts[0] });
    
            // Correct sender
            let result = await Etherpost.methods.getUploads(accounts[0]).call();
            // console.log(result);
            assert.strictEqual(result.length, 1, 'Expects 1 record from the sender');
            assert.strictEqual(result[0], hash1, 'Post hash matches from the sender'); 

            // Incorrect sender
            result = await Etherpost.methods.getUploads(accounts[1] /* wrong account */).call();
            // console.log(result);
            assert.strictEqual(result.length, 0, 'Expects no record from a wrong account');
        });

        it("should store multiple posts", async () => {
            const accounts = await web3.eth.getAccounts();
            await Etherpost.methods.upload(hash2).send({ from: accounts[0] });
    
            // Correct sender
            let result = await Etherpost.methods.getUploads(accounts[0]).call();
            // console.log(result);
            assert.strictEqual(result.length, 2, 'Expects 2 records from the sender');
            assert.strictEqual(result[0], hash1, 'Post hash matches from the sender'); 
            assert.strictEqual(result[1], hash2, 'Post hash matches from the sender'); 
        });

        it("should store posts for different senders", async () => {
            const accounts = await web3.eth.getAccounts();
            await Etherpost.methods.upload(hash3).send({ from: accounts[1] }); // add 1 post for account 2
    
            let result = await Etherpost.methods.getUploads(accounts[0]).call();
            // console.log(result);
            assert.strictEqual(result.length, 2, 'Expects 2 records from account[0] earlier');
            assert.strictEqual(result[0], hash1, 'Post hash matches from the sender'); 
            assert.strictEqual(result[1], hash2, 'Post hash matches from the sender'); 

            result = await Etherpost.methods.getUploads(accounts[1]).call();
            // console.log(result);
            assert.strictEqual(result.length, 1, 'Expects 1 records from account[1]');
            assert.strictEqual(result[0], hash3, 'Post hash matches from the sender');
        });

        it("should ignore identical hashes", async () => {
            const accounts = await web3.eth.getAccounts();
            await Etherpost.methods.upload(hash1).send({ from: accounts[3] });
            await Etherpost.methods.upload(hash1).send({ from: accounts[3] });
            
            // Only 1 record, not 2
            result = await Etherpost.methods.getUploads(accounts[3]).call();
            // console.log(result);
            assert.strictEqual(result.length, 1, 'Expects 1 record');
        });

        it("should raise log upload event", async () => {
            Etherpost.events.LogUpload((err, event) => {
                assert.ifError(err)  // no error
                // console.log(event)
                assert.strictEqual(event.event, 'LogUpload');
                assert.strictEqual(event.returnValues.uploader, accounts[4]);
                assert.strictEqual(event.returnValues.ipfsHash, hash1);
            });
            const accounts = await web3.eth.getAccounts();
            await Etherpost.methods.upload(hash1).send({ from: accounts[4] });
        });

    });
});