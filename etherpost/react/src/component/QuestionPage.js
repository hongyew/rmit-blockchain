import React, { Component } from 'react';
import '../App.css';
import { Container, Jumbotron, Row, Col, Button, Form } from 'react-bootstrap';
import IPFS from 'ipfs';
import Url from 'url-parse';
import IpfsTools from './ipfs_utils';
import EmbarkJS from '../embarkArtifacts/embarkjs';
import Etherpost from '../embarkArtifacts/contracts/Etherpost';
import ImageUpload from './ImageUpload';
import sha from 'sha.js';
import { CopyToClipboard } from 'react-copy-to-clipboard';


/**
 * Allows game master to setup a quiz.
 */
export default class QuestionPage extends Component {
  ipfs = null;
  commentInput = {}

  constructor(props) {
    super(props)
    this.ipfs = new IPFS()
    this.state = {
      imageHash: null,
      file: null,
      stage: 1,
    }
  }


 /**
   * Store file using Embark API.
   * 
   * There is a bug with Embark.Storage.get() API where it forces the content to be string on the way out.
   * So the safest way is to store the image as base64 string.
   */
  storeFile(file, callback) {
    const reader = new FileReader();
    reader.onloadend = callback
    reader.readAsArrayBuffer(file)
  }

  //------------------------------------------------------------------------------------------------------------------------
  // UI Event Handlers
  //------------------------------------------------------------------------------------------------------------------------
  onImageSubmitted(file) {
    this.setState({
      file: file,
      stage: 2
    })
  }

  async onSubmitQuiz(e) {
    e.preventDefault();

    // store the image file
    const reader = new FileReader();
    reader.onloadend = async () => {
      const buffer = Buffer.from(reader.result);
      
      // Save image into IPFS
      const ipfsHash = await EmbarkJS.Storage.saveText(buffer.toString('base64'))
      console.log(`Image stored in IPFS ${ipfsHash}`)
      const imageHash = IpfsTools.hashToHex32(ipfsHash); // convert from IPFS hash to byte32 hex

      // Store question in IPFS
      const data = this.questionInput.value;
      const quizIpfs = await EmbarkJS.Storage.saveText(data);

      // Calculate the hash of the answer
      const hashedAnswer = this.hashAnswer(this.answerInput.value);
      console.log('onSubmitQuiz(): Answer:', this.answerInput.value, hashedAnswer)

      // Estimate gas and submit quiz
      const quizContract = Etherpost.methods.quiz(imageHash, IpfsTools.hashToHex32(quizIpfs), hashedAnswer);
      const estimatedGas = Math.round((await quizContract.estimateGas({from: this.props.account })) * 1.1);
      quizContract.send({ from: this.props.account, gas: estimatedGas })
      .on('transactionHash', (hash) => {
        this.setState({ 
          quizIpfs: quizIpfs, 
          imageHash: imageHash,
          stage: 3
        });
      });
    }
    reader.readAsArrayBuffer(this.state.file)
  }

  hashAnswer(answer) {
    if (!answer) answer = ''  // make it empty string if it is null or undefined.
    answer = '' + answer      // Force string 
    return '0x' + sha('sha256').update(answer.toUpperCase()).digest('hex')
  }

  generateQuizUrl() {
    const url = new Url(window.location.href);
    return url.protocol + "//" + url.hostname + ((url.port)? ":" + url.port: "") + `/quiz/${this.props.account}/${this.state.imageHash}`
  }

  //------------------------------------------------------------------------------------------------------------------------
  // Rendering logic
  //------------------------------------------------------------------------------------------------------------------------

  renderUpload() {
    return (
    <div>
      <p>Let's start by uploading a picture:</p>
      <ImageUpload ipfs={this.props.ipfs} account={this.props.account} onImageSubmitted={this.onImageSubmitted.bind(this)}/>
    </div>);
  }

  renderQuiz() {
    return (
    <Form className="input-sm" onSubmit={this.onSubmitQuiz.bind(this)}>
      <Form.Group as={Row} controlId="formQuestion">
        <Form.Label column sm="3">Quiz Question</Form.Label>
        <Col sm="9"><Form.Control type="text" ref={(r) => this.questionInput = r} placeholder="eg. How many children in the picture?" /></Col>
      </Form.Group>
      <Form.Group as={Row} controlId="formAnswer">
        <Form.Label column sm="3">Quiz Answer</Form.Label>
        <Col sm="9"><Form.Control type="text" ref={(r) => this.answerInput = r} placeholder="eg. 7" /></Col>
      </Form.Group>
      <Button className="mt-2" type="submit" >Go</Button>
    </Form>);
  }

  renderLink() {
    const quizUrl = this.generateQuizUrl();
    return (
      <div>
        <p>Send this link to your friends to participate in the game.</p>
        <Form.Group as={Row} controlId="formQuestion">
          <Col sm="10">
            <Form.Control plaintext readOnly defaultValue={quizUrl} />      
          </Col>
          <Col sm="2">
            <CopyToClipboard text={quizUrl}><Button variant="outline-info" >Copy</Button></CopyToClipboard>
          </Col>
        </Form.Group>
        <Button style={{ marginLeft: '30%', marginRight: '30%', marginTop: '1rem'}} onClick={() => this.props.history.push(`/`) }>Return to Gallery</Button>
      </div>
    );
  }

  renderStages() {
    switch(this.state.stage) {
      case 1: return this.renderUpload();
      case 2: return this.renderQuiz();
      case 3: return this.renderLink();
      default: return this.renderUpload();
    }
  }

  render() {
    console.log(this.props)
    if (!this.props.accounts) return ''
    return (
      <Container>
        <Jumbotron>
          <h1>Welcome</h1>
          <p>
            This is a game where you upload a picture and ask a simple quiz about the picture.
            The system will generate a link for you to send to your friends, and see who
            is the quickest to get the correct answer.
          </p>
          {this.renderStages()}
        </Jumbotron>
      </Container>) 
  }
}


