# Blockchain Course

* Todo Exercise - Create a todo app
* Cryptopix - IPFS read/store


# Technical Documentation

Work for [RMIT Blockchain course](https://futureskills.rmit.edu.au/blockchain-courses) 2019.

Created using default ``embark new``.

## Solidity Version

Uses 0.4.24

* Add ``"solidity.compileUsingRemoteVersion": "v0.4.24+commit.e67f0147"`` to workspace settings.

* Modify solc version to ``"solc": "0.4.24"`` in ``embark.json``.

## Setup Software Dependencies

```
npm install -g embark
```

Note: At the time of writing, only use the LTS release of node (`n lts`). It was ``v10.16.0`` at the time.

## Build/Run

```
embark simulator
embark run
```
## Test

### Setup

Run ``npm install web3`` to support testing via Javascript. This is not provided by embark scafolding.

### Run

``embark test``

# Ehterpost App

### Account Setup

The application requires an Ethereum supported browser (eg. MetaMask).

The ``config/blockchain.js`` file has been modified with account information:

```
,accounts: [
      {
        mnemonic: process.env.METAMASK_MNEMONIC,
      },
      {
        privateKey: process.env.METAMASK_TEST2_PK,
      },
    ]

```
The application expects METAMASK_MNEMONIC and METAMAST_TEST2_PK to be setup in the environment.

### Embark Port

The config/contracts.js deployment port has been modified to 8556 (default was 8545). It depends on which port your blockchain (or simulator) runs on.

### IPFS

IPFS should be running

```
ipfs daemon
```

### Running the app

Assuming you have the simulator running, if not ``embark simulator``:

```
cd etherpost
npm run build
cd react
npm install
npm start
```

The ``npm run build`` executes ``embark build`` which deploys the contract and copies the build contract JSON files to the react/contracts folder. 



