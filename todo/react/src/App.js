import React, { Component } from 'react'
import Web3 from 'web3'
import './App.css'
import { Container, Row, ListGroup, Button, Form } from 'bootstrap-4-react';
import TodoContract from './contracts/TodoList.json';  // Copied from /dist/contracts. See 'npm run link'

/**
 * NOTE: 
 * https://web3js.readthedocs.io/en/1.0/web3-eth-contract.html#methods-mymethod-send
 * Cannot use async/await for send(). Only on('transactionHash',..) is what returns immediately.
 * 
 */
class App extends Component {
  web3 = null;
  todoService = null;

  constructor(props) {
    super(props)
    this.taskInput = React.createRef();
    this.state = {
      account: '',
      taskCount: 0,
      tasks: [],
    }
  }

  async componentWillMount() {
    await this.initBlockchain();
    await this.loadTasks();
  }

  componentDidMount() {
  }

  async initBlockchain() {
    // const web3 = new Web3(Web3.givenProvider || "http://localhost:8556")
    this.web3 = new Web3("http://localhost:8556")
    const accounts = await this.web3.eth.getAccounts()
    this.todoService = new this.web3.eth.Contract(TodoContract.abiDefinition, TodoContract.deployedAddress);    
    this.setState({ 
      account: accounts[0]
    })

    console.log(`Deployed address: ${TodoContract.deployedAddress}`);
    console.log(`Account: ${accounts}`);
  }

  async loadTasks() {
    const tasks = await this.todoService.methods.get().call();
    console.log(tasks);
    this.setState({ 
      tasks: (tasks != null)? tasks: this.state.tasks
    })
  }
 
  findTargetId(formTarget, id) {
    for (let i=0; i<formTarget.length; i++) {
      if (formTarget[i].id === id) return formTarget[i];
    }
    return undefined;
  }

  /** 
   * e.target will contains an array of <form> nodes. 
   */ 
  async onSubmit(e) {
    e.preventDefault();
    let field = this.findTargetId(e.target, 'addTaskInput');
    if (field && field.value) {
      let estimatedGas = await this.todoService.methods.add(field.value).estimateGas({from: this.state.account, to: TodoContract.deployedAddress});
      console.log(`Estimated gas: ${estimatedGas}`);
      this.todoService.methods.add(field.value).send({ from: this.state.account , gas: Math.round(estimatedGas * 1.1)})
        .on('transactionHash', (hash) => {
          console.log(`add() received: ${hash}`);
          this.loadTasks();
          field.value = null;
        });
    }
  }

  onTaskCheck(i) {
    let tasks = this.state.tasks;
    tasks[i].completed = !this.state.tasks[i].completed; // toggle it 
    console.log(tasks[i]);

    // Upload it to the blockchain
    let action = (tasks[i].completed)? this.todoService.methods.done(i): this.todoService.methods.undo(i);
    action.send({ from: this.state.account }).on('transactionHash', (hash) => {
      console.log(`task status change received: ${hash}`);
      // this.setState({ tasks: tasks }); // We could have updated state locally, but is better to check from the blockchain
      this.loadTasks();
    });
    return true;
  }

  clearAll() {
    this.todoService.methods.clearAll().send({ from: this.state.account })
    .on('transactionHash', (hash) => {
      console.log(`clearAll() received: ${hash}`);
      this.loadTasks();
    });
  }

  taskList() {
    // mapping key to array index is anti-pattern, but should be ok for now.
    return this.state.tasks.map((task, index) => 
    <ListGroup.Item key={index}>
      <input type='checkbox' defaultChecked={task.completed} onChange={() => this.onTaskCheck(index)}/> {task}
    </ListGroup.Item>);
  }

  render() {
    //this.dump();
    return (
      <Container>
        <p>Contract Address: {TodoContract.deployedAddress}</p>
        <p>Your account: {this.state.account}</p>
        <ListGroup>{this.taskList()}</ListGroup>
        <br/>
        <Form onSubmit={this.onSubmit.bind(this)}>
          <Form.Input id='addTaskInput' placeholder="enter your new task"/>
          <br/>
          <Button type='submit' primary>Add</Button>
          <Button type='button' secondary onClick={this.clearAll.bind(this)}>Clear all</Button>
        </Form>        
      </Container>
    );
  }
}

export default App;