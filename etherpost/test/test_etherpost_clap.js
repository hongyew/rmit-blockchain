
const Etherpost = require('Embark/contracts/Etherpost');
const sha = require('sha.js')

// For documentation please see https://embark.status.im/docs/contracts_testing.html
config({
    contracts: {
        "Etherpost": {}
    }
}, (_err) => {

});

// Tests - Note that the tests are stateful. SO it must be executed
// in sequence.
contract("Etherpost Claps", () => {

    // Test claps. Assuming that the blockchain already has the image hashes from
    // earlier tests.
    describe("Assuming there is an image hash", async () => {
        const hash1 = web3.utils.randomHex(32);

        // Add a clap to hash1, so the result should be 1.
        it("should have no claps in the beginning", async () => {
            const result = await Etherpost.methods.getClapCount(hash1).call();
            assert.strictEqual(result, "0"); // uint becomes a string in Javascript
        });

        // When the game has not been won yet, no one can clap. The call should fail.
        it("cannot clap until the game has been won", async () => {
            assert.rejects(async () => {
                await Etherpost.methods.clap(hash1).send();
            });
        });
    });

    // Scenarios for clapping after the game has won
    describe("When the game has been won", async () => {
        const hash8 = web3.utils.randomHex(32); 
        const hash9 = web3.utils.randomHex(32);
        const answer = '10';
        const answerHash1 = '0x' + sha('sha256').update(answer).digest('hex')
        const answerIpfs = web3.utils.randomHex(32);

        // Setup a quiz and answer, and flag it that it is won by account-3 
        before(async () => {
            const accounts = await web3.eth.getAccounts();

            // hash8 has been won by account-2
            await Etherpost.methods.upload(hash8).send({ from: accounts[1] });
            await Etherpost.methods.quiz(hash8, hash8, answerHash1).send({ from: accounts[1] });
            await Etherpost.methods.submitAnswer(hash8, answer, answerIpfs).send({ from: accounts[2] }); // can't be from the game host

            // hash9 has been won by account-3
            await Etherpost.methods.upload(hash9).send({ from: accounts[1] });
            await Etherpost.methods.quiz(hash9, hash9, answerHash1).send({ from: accounts[1] });
            await Etherpost.methods.submitAnswer(hash9, answer, answerIpfs).send({ from: accounts[3] }); // can't be from the game host
        })

        //Add a clap to hash1, so the result should be 1.
        it("should allow me to add a clap", async () => {
            await Etherpost.methods.clap(hash9).send();

            const result = await Etherpost.methods.getClapCount(hash9).call();
            assert.strictEqual(result, "1"); // uint becomes a string in Javascript
        });

        // Add another clap to hash1, so the result should be 2
        it("should allow me to add a multiple claps", async () => {
            await Etherpost.methods.clap(hash9).send();

            const result = await Etherpost.methods.getClapCount(hash9).call();
            assert.strictEqual(result, "2"); // uint becomes a string in Javascript
        });

        // Using account-3 as the winner of hash9
        it("should not allow the winner to clap its own win", async () => {
            const accounts = await web3.eth.getAccounts();
            assert.rejects(async () => {
                await Etherpost.methods.clap(hash9).send({ from: accounts[3] }); // account-3 is the game winner
            });
        });

        // The winner of hash9 can clap non-hash9
        it("should not allow the winner to clap its own win", async () => {
            const accounts = await web3.eth.getAccounts();
            await Etherpost.methods.clap(hash8).send({ from: accounts[3] }); // account-3 is not winner of hash8
            const result = await Etherpost.methods.getClapCount(hash8).call();
            assert.strictEqual(result, "1"); // uint becomes a string in Javascript
        });
    });
});