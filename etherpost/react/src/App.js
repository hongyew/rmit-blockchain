import React, { Component } from 'react';

import { BrowserRouter as Router, Route } from "react-router-dom"
import './App.css';
import { Navbar, Alert } from 'react-bootstrap';
import IPFS from 'ipfs';
import EmbarkJS from './embarkArtifacts/embarkjs';
import QuestionPage from './component/QuestionPage';
import GalleryPage from './component/GalleryPage';
import QuizPage from './component/QuizPage';
import AccountSelector from './component/AccountSelector'
/**
 * This is used by all pages based on the Route rule. 
 * To initializes all common page setup logic.
 * 
 * The Web3 instance is automatically by Embark.js (global.web3 and global.Web3).
 * See the embarkArtifacts/embarkjs.js file.
 * 
 * - web3             EmbarkJS.Blockchain.blockchainConnector.getInstance()
 * - accounts         EmbarkJS.Blockchain.blockchainConnector.getAccounts()
 * - default account  EmbarkJS.Blockchain.blockchainConnector.getDefaultAccount()
 */
export default class App extends Component {
  
  constructor(props) {
    super(props);
    this.state = { 
      embarkJSReady: false, 
      ipfsReady: false,
      ipfs: null,
      alert: null,
      accounts: null,
      account: null
    }
  }

  async componentDidMount() {
    if (window.ethereum !== 'undefined') {
      const accounts = await this.initProvider();
      this.setState({ 
        account: accounts[0],
        accounts: accounts
      })
    }
    else {
      console.error('No ethereum supported browser (window.ethereum)');
      this.setState({ alert: 'Your browser does not support Ethereum. Try MetaMask.' });
      return;
    }

    const ipfs = new IPFS()
    ipfs.once('ready',() => this.setState({ ipfsReady: true, ipfs: ipfs }))

    // If you want to use MetaMask, you should call window.ethereum.enabled()
    // before asking EmbarkJS.ready().
    EmbarkJS.onReady(async (error) => {
        if (error) {
            console.error('Error while connecting to web3', error);
            this.setState({ alert: 'Cannot connect to a suitable blockchain' });
        }
        else {
          this.setState({ 
            embarkJSReady: true 
          })
          this.keepAlive()
        }
    });
  }

  /**
   * Initialize with a provider, such as MetaMask.
   */
  async initProvider() {
    console.log('App.initProvider(): Provider detected');
    const accounts = await window.ethereum.enable();

    // register account changes, reload the page
    window.ethereum.on('accountsChanged',() => window.location.reload()); 

    return accounts;
  }

  /**
   * No provider detected. Use for local testing/debugging.
   */
  async initLocalHost() {
    console.log('App.initLocalHost(): Connecting to blockchain directly...');
    return await EmbarkJS.Blockchain.blockchainConnector.getAccounts()
  }

  /**
   * This is a hack to get around a problem/bug with webjs. It seems that after awhile,
   * if there is no blockchain activity, any calls to the blockchain web3.eth.XXX() will not resolve.
   * 
   * If I keep making a call periodically, it seems to prevent the problem. So this is to
   * the connection alive. Not sure what the cause is.
   */
  async keepAlive() {
    try {
      console.log('keepAlive()...')
      await EmbarkJS.Blockchain.blockchainConnector.getAccounts()
      setTimeout(this.keepAlive.bind(this), 20000)  
    }
    catch(e) {
      console.error(e)
    }
  }


  onAccountChanged(account) {
    this.setState({ account: account });
  }

  renderAlert() {
    return (this.state.alert)? <Alert style={{ margin: '1rem' }} variant='danger'>{this.state.alert}</Alert>:''
  }

  isReady = () => this.state.embarkJSReady && this.state.ipfsReady;

  renderRoutes() {
    return (
    <Router>
      <Route exact path="/" render={(routeProps) => (<GalleryPage {...routeProps} {...this.state} />)} />
      <Route exact path="/create" render={(routeProps) => (<QuestionPage {...routeProps} {...this.state} />)} />
      <Route path="/quiz/:account/:imageHash" render={(routeProps) => (<QuizPage {...routeProps} {...this.state} />)} />
    </Router>)
  }

  render() {
      return(<div>
          <Navbar bg="dark" variant="dark" expand="lg" style={{ marginBottom: '1rem' }}>
              <Navbar.Brand href="#home">Etherpost</Navbar.Brand>
              <Navbar.Toggle aria-controls="responsive-navbar-nav" />
              <Navbar.Collapse expand="sm" className="justify-content-end">
                <Navbar.Text>Account </Navbar.Text>
                {(this.isReady())? <AccountSelector accounts={this.state.accounts} selected={this.state.account} onAccountChanged={this.onAccountChanged.bind(this)} />:''}
              </Navbar.Collapse>
          </Navbar>
          {this.renderAlert()}
          {(this.isReady())? this.renderRoutes(): 'Loading...'}
      </div>)
  }
}
