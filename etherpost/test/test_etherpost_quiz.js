
const Etherpost = require('Embark/contracts/Etherpost');
const sha = require('sha.js')

// For documentation please see https://embark.status.im/docs/contracts_testing.html
config({
    contracts: {
        "Etherpost": {}
        }
    }, (_err) => {
});

// Tests - Note that the tests are stateful. SO it must be executed
// in sequence.
contract("Etherpost Quiz", () => {

    // Test claps. Assuming that the blockchain already has the image hashes from
    // earlier tests.
    describe("Assuming there is an image hash", async () => {
        const imageHash = web3.utils.randomHex(32);
        const quizHash1 = web3.utils.randomHex(32);
        const answer = 'labrador'.toUpperCase();
        const answerHash1 = '0x' + sha('sha256').update(answer).digest('hex')
        const answerIpfs = web3.utils.randomHex(32);

        it("should allow game host to post a quiz & answer", async () => {
            const accounts = await web3.eth.getAccounts();
            await Etherpost.methods.quiz(imageHash, quizHash1, answerHash1).send({ from: accounts[1] })
            const result = await Etherpost.methods.getQuiz(imageHash).call();
            assert.strictEqual(result, quizHash1);
        });

        // account-3 attempts to submit answer, but will fail.
        it("should allow anyone to verify the incorrect answer", async () => {
            const accounts = await web3.eth.getAccounts();
            const result = await Etherpost.methods.submitAnswer(imageHash, 'WRONG', answerIpfs).send({ from: accounts[3] /* not game host */});
            assert.strictEqual(result.events.LogAnswer.returnValues.result, false);
        });

        // there is no winner yet
        it("should not return any winner if the game is not won yet", async () => {
            const result = await Etherpost.methods.getWinner(imageHash).call();
            assert.strictEqual(parseInt(result, 16), 0 /* no winner result is '0x0000....' in hex */);
        });      

        // there is no winner yet
        it("should not allow anyone reveal the answer when the game is not over", async () => {
            assert.rejects(async () => {
                await Etherpost.methods.revealAnswer(imageHash).call();
            })
        });    

        // account-2 becomes the winner by submitting the correct answer. account-2 will become the winner.
        it("should register the winning result", async () => {
            const accounts = await web3.eth.getAccounts();
            const result = await Etherpost.methods.submitAnswer(imageHash, answer, answerIpfs).send({ from: accounts[2] /* not game host */});
            assert.strictEqual(result.events.LogAnswer.returnValues.result, true);
        });

        // the winner should be account-2 from the previous step
        it("should return the winner once the game has been won", async () => {
            const accounts = await web3.eth.getAccounts();
            const result = await Etherpost.methods.getWinner(imageHash).call();
            assert.strictEqual(result, accounts[2]);
        });      

        it("should allow anyone reveal the answer once the game is over", async () => {
            const result = await Etherpost.methods.revealAnswer(imageHash).call();
            assert.strictEqual(result, answerIpfs);
        });       

        // it("should raise LogAnswer event when submitting answer", async () => {
        //     Etherpost.events.LogAnswer((err, event) => {
        //         assert.ifError(err)  // no error
        //         //console.log(event)
        //         assert.strictEqual(event.event, 'LogAnswer');
        //         assert.strictEqual(event.returnValues.imageHash, imageHash);
        //         assert.strictEqual(event.returnValues.result, false);
        //         Etherpost.events.LogAnswer();
        //     });
        //     await Etherpost.methods.submitAnswer(imageHash, 'WRONG', answerIpfs).send(); // don't submit right answer yet
        // });
    });
});