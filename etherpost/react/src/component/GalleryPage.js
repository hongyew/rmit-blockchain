import React, { Component } from 'react';
import '../App.css';
import { Container, Row, Col, Button } from 'react-bootstrap';
import IpfsTools from './ipfs_utils';
import EmbarkJS from '../embarkArtifacts/embarkjs';
import Etherpost from '../embarkArtifacts/contracts/Etherpost';
import Gallery from 'react-grid-gallery';
/**
 * Gallery page for the current account
 */
export default class GalleryPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activeImages: [],
      finishedImages: [],
    }
  }

  //------------------------------------------------------------------------------------------------------------------------
  // Rendering logic
  //------------------------------------------------------------------------------------------------------------------------

  async componentDidMount() {
      await this.loadImages();
      Etherpost.events.LogUpload(this.onUpLoadEvent.bind(this));
  }

  componentWillUnmount() {
    Etherpost.events.LogUpload(null);
  }
  
  async readFile(hash) {
    if (hash === null) return;
    console.log(`EtherPicture.readFile(${hash}) ...`);
    const buffer = await EmbarkJS.Storage.get(hash); // Output is forced to be a string
    console.log(`EtherPicture.readFile(${hash}): read ${buffer.length} bytes`);
    return {
      imageData: 'data:;base64,' + buffer,
      hash: hash
    };
  }

  async loadImages() {
    console.log('loadImages()')
    const imagesHex32 = await Etherpost.methods.getUploads(this.props.account).call(); /* array of hex32 */
    const activeImages = [], finishedImages = []

    console.log('loadImages():', imagesHex32)
    for (let i in imagesHex32) {
      const winner = await Etherpost.methods.getWinner(imagesHex32[i]).call();
      const galleryData = await this.createGalleryImageData(imagesHex32[i]);
      console.log(galleryData)
      if (winner > 0) {
        finishedImages.push(galleryData);
      }
      else {
        activeImages.push(galleryData);        
      }
    }
    
    this.setState({ 
      activeImages: activeImages, 
      finishedImages: finishedImages 
    })
  }

  async createGalleryImageData(imagesHex32) {
    const ipfsHash = IpfsTools.hex32ToHash(imagesHex32)
    const b64ImageData = await this.readFile(ipfsHash);

    return {
      thumbnail: b64ImageData.imageData,
      src: b64ImageData.imageData,
      thumbnailWidth: 0,
      thumbnailHeight: 0,
      hex32Hash: imagesHex32,
      ipfsHash: ipfsHash,
    }
  }

  /**
   * event LogUpload(address uploader, bytes32 ipfsHash, uint timestamp);
   */
  async onUpLoadEvent(err, event) {
    // Evaluate to integer because the 'event.returnValues.uploader' hex is mixed case, which is
    // different from 'this.props.account', string matching wise.
    console.log("onUpLoadEvent():", parseInt(event.returnValues.uploader,16) , parseInt(this.props.account, 16), (parseInt(event.returnValues.uploader,16) === parseInt(this.props.account, 16)))
    if (parseInt(event.returnValues.uploader,16) === parseInt(this.props.account, 16)) {
      console.log("onUpLoadEvent():", event)
      
      // The gallary widget didn't work with setState changes for some reason.
      // So force the page to refresh.
      window.location.reload()
    }
  }

  onImageClicked(imageHex32) {
    this.props.history.push(`/quiz/${this.props.account}/${imageHex32}`)
  }

  onCreate() {
    this.props.history.push(`/create`)
  }

  renderGallery(images) {
    if (images.length === 0) return (<center style={{ margin: '32px'}}>No images</center>)
    return (
      <Gallery images={images} enableLightbox={false} 
               onClickThumbnail={(i) => this.onImageClicked(images[i].hex32Hash)}/>
    )
  }

  render() {
    return (
      <Container>
        <Button onClick={this.onCreate.bind(this)}>Make a game</Button>
        <Row style={{ marginTop: '1rem'}}>
          <Col>
            <h5>Your Active Games</h5>
            {this.renderGallery(this.state.activeImages)}
         </Col>
        </Row><Row style={{ marginTop: '1rem'}}>
          <Col>
            <h5>Your Finished Games</h5>
            {this.renderGallery(this.state.finishedImages)}
          </Col>
        </Row>
      </Container>) 
  }
}


