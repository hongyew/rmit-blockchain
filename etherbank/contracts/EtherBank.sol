pragma solidity 0.4.24;

contract EtherBank {
    address public owner = msg.sender;
    event Log (address sender, string method);

    // Unnamed function is called when someone try to send ether to this addres
    function () public payable {
        emit Log(msg.sender, 'unnamed');
    }

    function withdraw () public {
        msg.sender.transfer(address(this).balance);
    }

    function balance() public view returns (uint) {
        return address(this).balance;
    }
}