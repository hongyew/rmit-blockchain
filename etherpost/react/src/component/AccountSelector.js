import React, { Component } from 'react';
import '../App.css';
import { NavDropdown } from 'react-bootstrap';
import PropTypes from 'prop-types';

/**
 * accounts
 * onAccountChanged
 */
export default class AccountSelector extends Component {
  static propTypes = {
    accounts: PropTypes.array,
    selected: PropTypes.string,
    onAccountChanged: PropTypes.func
  };

  constructor(props) {
    super(props)
    this.state = {
      selected: (props.selected)? props.selected: ((props.accounts && props.accounts.length > 0)? props.accounts[0]: null)
    }
  }

  onAccountChanged(e) {
    const account = e.target.innerText;
    this.setState({ selected: account });
    if (this.props.onAccountChanged) this.props.onAccountChanged(account);
  }

  static getDerivedStateFromProps(props, state) {
    console.log('getDerivedStateFromProps')
    state.selected = props.selected;
    return state;
  }
  
  render() {
    const item = (name) => 
    <NavDropdown.Item onClick={this.onAccountChanged.bind(this)} key={name} eventKey={name}>{name}</NavDropdown.Item>
    return (
      <NavDropdown variant="dark" title={(this.state.selected)? this.state.selected: 'Select your Ether account'} id="basic-nav-dropdown">
        {this.props.accounts.map((x) => item(x))}
      </NavDropdown>
    )
  }

  // render() {
  //     const item = (name) => 
  //     <Dropdown.Item onClick={this.onAccountChanged.bind(this)} key={name} eventKey={name}>{name}</Dropdown.Item>
  //     return (
  //       <Container style={{ margin: '1rem' }}>
  //         <DropdownButton id="dropdown-basic-button" 
  //             title={(this.state.selectedAccount)? this.state.selectedAccount: 'Select your Ether account'}>
  //         {this.props.accounts.map((x) => item(x))}
  //         </DropdownButton>
  //       </Container>
  //     )
  //   }
}