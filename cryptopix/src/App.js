import React, { Component } from 'react';
import './App.css';
import { Container, Row, Col, Button, Form, ListGroup } from 'react-bootstrap';
import IPFS from 'ipfs';
import buffer from 'buffer';

/**
 * IPFS public gateway https://ipfs.github.io/public-gateway-checker/
 * Bootstraps style spacing: https://mdbootstrap.com/docs/react/utilities/spacing/
 */
class App extends Component {
  ipfs = null;
  
  constructor(props) {
    super(props)
    this.ipfs = new IPFS()
    console.log(this.ipfs);
    this.state = {
      ipfsFiles: [],
      readBack: null,
      fileHash: null
    }
  }

  componentDidMount() {
    this.readFile(this.state.fileHash);
  }

  async listIpfs(path) {
    try {
      let files = await this.ipfs.ls(path)
      console.log(`listIpfs(): ${files.length} files`);
      this.setState({
        ipfsFiles: files
      });  
    }
    catch(err) {
      console.log(`Error listing IPFS ${err}`)
    }
  }

  findTargetId(formTarget, id) {
    for (let i=0; i<formTarget.length; i++) {
      if (formTarget[i].id === id) return formTarget[i];
    }
    return undefined;
  }

  onHashSubmit(e) {
    e.preventDefault();
    let field = this.findTargetId(e.target, 'hashInput');
    if (field) {
      console.log(`onHashSubmit(): ${field.value}`);
      this.readFile(field.value)
    }
  }

  onPictureSubmit(e) {
    e.preventDefault();
    let field = this.findTargetId(e.target, 'fileInput');
    if (field && field.files.length > 0) { 
      // https://developer.mozilla.org/en-US/docs/Web/API/File/Using_files_from_web_applications
      console.log(field.files[0]);
      this.storeFile(field.files[0])
    }
  }

  async readFile(hash) {
    if (hash === null) return;
    try {
      const path = '/ipfs/' + hash
      console.log(`readFile(${path})`);
      const buffer = await this.ipfs.cat(path);
      console.log(`readFile(${path}): read ${buffer.length} bytes`);
      this.setState({
        readBack: 'data:image/jpeg;base64,' + buffer.toString('base64'),
        fileHash: hash
      });
    } catch(e) {
      console.error(`readFile(): ${e}`);
    }
  }

  storeFile(file) {
    const reader = new FileReader();
    reader.onload = () => {
      const buf = buffer.Buffer(reader.result)
      console.log(buf);
      this.ipfs.add({ path: file.name, content: buf}, (err, result) => {
        if (err) {
            console.error(`Error writing to IPFS ${err}`)
            return
        }
        console.log(`File stored in IPFS ${JSON.stringify(result[0])}`)
        this.readFile(result[0].hash);
      });
    }
    reader.readAsArrayBuffer(file);
  }

  renderUpload() {
    return (
    <Form className="p-2" onSubmit={this.onPictureSubmit.bind(this)}>
      <Form.Control type="file" id="fileInput" ></Form.Control>
      <Button className="mt-2" type="submit" >Upload</Button>
    </Form>);
  }

  renderImage() {
    return (this.state.readBack)? <img alt='content stored in IPFS' src={this.state.readBack}></img>: ''
  }

  renderIpfsFiles() {
    if (this.state.ipfsFiles) {
      return (this.state.ipfsFiles.map((file, index) => <ListGroup.Item key={index}>{file.name}</ListGroup.Item>));
    }
    else {
      return '';
    }
  }

  renderHashInput() {
    return (<Form onSubmit={this.onHashSubmit.bind(this)}>
      <Form.Control type='text' id='hashInput' placeholder='enter IPFS file hash'></Form.Control>
      <Button className="mt-2" type='submit'>Retrieve Image</Button>
    </Form>);
  }

  renderImageUpload() {
    return (
      <Container>
        <p>Browser IPFS</p>
        {this.renderImage()}
        <p>{this.state.fileHash}</p>      
        {this.renderUpload()}
      </Container>) 
  }

  render() {
    return (
      <Container>
        <p>Using browser IPFS</p>
        {this.renderImage()}
        <p>{this.state.fileHash}</p>      
        {this.renderUpload()}
        {this.renderHashInput()}
      </Container>) 
  }
}


export default App;
