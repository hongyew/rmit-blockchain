# CryptoPix

## Setup

```
npm install
```

## Provider (MetaMask) Setup

In ``blockchain.json``, set the mnemonic and balance to pre-fill the provider (eg. MetaMash) gas and accounts for testing.

```
    ,accounts: [
      {
        mnemonic: "xxx xxx xxx....",
        balance: '999 ether'
      }
    ]
```

## Run

This is a React client-side Javascript application

```
npm start
```

Visit http://localhost:3000

## Development

This code uses the Embark framework and assumes there is a provider like MetaMask.

### Obtain account(s)

From provider (like MetaMask), this should returns an array of accounts.
```
accounts = await window.ethereum.enable();
```

### Read data from a contract using method xxx()

```
const data = await Etherpost.methods.xxx().call()
```

### Write data to contract using method xxx()

```
await Etherpost.methods.xxx(data).send({ from: account, gas: ?? })
```

If you need quicker feedback, the 'transactionHash' event will return most quickly pending for transaction to be fully mined:

```
Etherpost.methods.xxx(data).send({ from: account, gas: ?? })
.on('transactionHash', (hash) => {
    console.log(hash)
})
```

The gas to be offered can be custom entered, or could be estimated by:

```
let estimatedGas = await Etherpost.methods.upload(hash).estimateGas({from: account, to: PostContract.deployedAddress})
let offerGas = Math.round(estimatedGas * 1.1)        
```

### Contract Type and Javascript 

Often contract function parameters/return may be uint/bytes (unit32, bytes32, and other variations etc), but they are marshell as string in Javascript.

* uint - as string 
* bytes - hex string '0xXXXXX'

### Working around IPFS has byte32 from Contract in Javascript

To store IPFS hash as efficient byte32, see ipfs_utils helper functions. 

See
https://learn.rmitonline.edu.au/courses/course-v1:RMIT+BLC202+2019_JUN_B/courseware/7c5fbbd51bb14156be3b6f831f459922/3711d0ff7e524db68f3c469852d2d67f/?activate_block_id=block-v1%3ARMIT%2BBLC202%2B2019_JUN_B%2Btype%40sequential%2Bblock%403711d0ff7e524db68f3c469852d2d67f

