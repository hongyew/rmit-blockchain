pragma solidity 0.4.24;

contract EtherpostInterface {
    event LogUpload(address uploader, bytes32 ipfsHash, uint timestamp);
    event LogClap(address clapper, bytes32 ipfsHash, uint count);
    event LogComment(address commenter, bytes32 imageHash, bytes32 commentHash, uint timestamp);

    function upload(bytes32 ipfsHash) public;
    function getUploads(address uploader) public returns(bytes32[]);
    function clap(bytes32 ipfsHash) public;
    function getClapCount(bytes32 ipfsHash) public returns(uint);
    function comment(bytes32 imageHash, bytes32 commentHash) public;
    function getComments(bytes32 imageHash) public returns(bytes32[]);
}

contract Etherpost is EtherpostInterface {
    event LogAnswer(address sender, bytes32 imageHash, bool result, bytes32 answerIpfs, uint timestamp);

    struct Quiz {
        bytes32 quizHash;
        bytes32 answerHash;
        address winner;
        bytes32 answerIpfs;
    }

    // user -> photo ipfs hash
    mapping(address => bytes32[]) photoHash;

    // photoHash -> comment ipfs hash
    mapping(bytes32 => bytes32[]) commentsHash;

    // photoHash -> claps count
    mapping(bytes32 => uint) claps;

    // photoHash -> game quiz
    mapping(bytes32 => Quiz) quizes;

    /**
     * Upload hash. Idempotent operation.
     */
    function upload(bytes32 ipfsHash) noDuplicates(ipfsHash) public {
        photoHash[msg.sender].push(ipfsHash);
        emit LogUpload(msg.sender, ipfsHash, now);
    }

    function getUploads(address uploader) public returns(bytes32[]) {
        return photoHash[uploader];
    }

    function clap(bytes32 ipfsHash /* photo hash */) public noWinner(ipfsHash) onlyAtGameOver(ipfsHash) {
        if (claps[ipfsHash] > 254) return; // don't clap over 255 (overflow)
        claps[ipfsHash] = claps[ipfsHash] + 1;
        emit LogClap(msg.sender, ipfsHash, claps[ipfsHash]);
    }

    function getClapCount(bytes32 ipfsHash /* photo hash */) public returns(uint) {
        return claps[ipfsHash];
    }

    function comment(bytes32 imageHash, bytes32 commentHash) public noGameHost(imageHash) {
        commentsHash[imageHash].push(commentHash);
        emit LogComment(msg.sender, imageHash, commentHash, now);
    }

    function getComments(bytes32 imageHash) public returns(bytes32[]) {
        return commentsHash[imageHash];
    }

    function quiz(bytes32 imageHash, bytes32 quizHash, bytes32 answerHash) public {
        upload(imageHash);
        quizes[imageHash].winner = address(0);
        quizes[imageHash].answerIpfs = 0x0;
        claps[imageHash] = 0;
        delete commentsHash[imageHash];
        quizes[imageHash].quizHash = quizHash;
        quizes[imageHash].answerHash = answerHash;
    }

    function getQuiz(bytes32 imageHash) public view returns(bytes32) {
        return quizes[imageHash].quizHash;
    }

    /**
     * Caller must use send() to call this contract, and the return result (true/false) can be obtained by:
     *
     * 1. LogAnswer event, or
     * 2. the return object: {returnObject}.events.LogAnswer.returnValues.result
     *
     * This function acts like a comment() also takes the raw text in addition to the ipfs hash.
     * It will sha2() hash the raw text for answer verification.
     */
    function submitAnswer(bytes32 imageHash, string text, bytes32 answerIpfs) public noGameHost(imageHash) gameNotOver(imageHash) {
        commentsHash[imageHash].push(answerIpfs);

        bytes32 hashedAnswer = sha256(abi.encodePacked(text));
        bool result = (quizes[imageHash].answerHash == hashedAnswer);
        if (result) {
            quizes[imageHash].winner = msg.sender;
            quizes[imageHash].answerIpfs = answerIpfs;
        }
        emit LogAnswer(msg.sender, imageHash, result, answerIpfs, now);
    }

    function revealAnswer(bytes32 imageHash) public onlyAtGameOver(imageHash) view returns(bytes32) {
        return quizes[imageHash].answerIpfs;
    }

    function getWinner(bytes32 imageHash) public view returns(address) {
        return quizes[imageHash].winner;
    }

    function resetGame(bytes32 imageHash) public onlyGameHost(imageHash) {
        quizes[imageHash].winner = address(0);
        quizes[imageHash].answerIpfs = 0x0;
        claps[imageHash] = 0;
        delete commentsHash[imageHash];
    }

    /**
     * Originally, I had it abort on duplicated post, but now I have
     * decided to not raise error and just return silently.
     */
    modifier noDuplicates(bytes32 hash) {
        uint count = photoHash[msg.sender].length;
        for (uint i = 0; i < count; i++) {
            if (photoHash[msg.sender][i] == hash) return /* don't forece require(), but return */;
        }
        _;
    }

    /**
     * The game host cannot call this
     */
    modifier noGameHost(bytes32 hash) {
        uint count = photoHash[msg.sender].length;
        for (uint i = 0; i < count; i++) {
            require(photoHash[msg.sender][i] != hash, 'The game host cannot use this');
        }
        _;
    }

    /**
     * Only the game host can call this
     */
    modifier onlyGameHost(bytes32 hash) {
        bool matched = false;
        uint count = photoHash[msg.sender].length;
        for (uint i = 0; i < count; i++) {
            if (photoHash[msg.sender][i] == hash) {
                matched = true;
                _;
                break;
            }
        }
        require(matched, 'Only the game host can use this');
    }

    modifier noWinner(bytes32 hash) {
        require(quizes[hash].winner != msg.sender, 'The winner cannot use this');
        _;
    }

    modifier onlyAtGameOver(bytes32 hash) {
        require(quizes[hash].winner != address(0), 'Cannot do this until the game is over');
        _;
    }

    modifier gameNotOver(bytes32 imageHash) {
        require(quizes[imageHash].winner == address(0), 'The game is finished');
        _;
    }
}
