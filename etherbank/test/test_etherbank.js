
const EtherBank = require('Embark/contracts/EtherBank');

// For documentation please see https://embark.status.im/docs/contracts_testing.html
config({
    contracts: {
        "EtherBank": {}
    }
}, (_err) => {

});

// Tests - Note that the tests are stateful. SO it must be executed
// in sequence.

// Some useful embark properties:
//   EtherBank.address   - contract address
//   EtherBank.gas       - contract gas balance
//
contract("EtherBank", () => {
   describe("Given I have EtherBank contract", async () => {
        // Onwer is msg.sender at constructor, which is the same as 'defaultAccount' from Embark
        it("The owner is set correctly", async () => {
            const owner = await EtherBank.methods.owner().call();
            console.log(`      Owner: ${owner}`);
            console.log(`      EtherBank.defaultAccount: ${EtherBank.defaultAccount}`)
            assert.strictEqual(owner, EtherBank.defaultAccount)
        });

        it("I can transfer gas to the contract", async () => {
            const accounts = await web3.eth.getAccounts();

            // Contract address
            // https://web3js.readthedocs.io/en/v1.2.0/web3-eth-contract.html#options-address
            console.log(`      Contract address: ${EtherBank.options.address}`)

            // Send a value to contract address, which calls the unnamed method
            // and automatically updates the account balance
            const result = await web3.eth.sendTransaction({
                from: accounts[0],
                to: EtherBank.options.address,
                value: 100
            });

            // Check the contract account balance.
            const balance = await EtherBank.methods.balance().call();
            assert.strictEqual(parseInt(balance), 100)
        });
    });
});