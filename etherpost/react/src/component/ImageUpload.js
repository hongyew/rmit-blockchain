import React, { Component } from 'react';
import '../App.css';
import { Form, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
/**
 * accounts
 * ipfs
 * onAccountChanged
 */
export default class ImageUpload extends Component {
  static propTypes = {
    account: PropTypes.string,
    ipfs: PropTypes.object,
    onImageSubmitted: PropTypes.func
  };

  onPictureSubmit(e) {
    e.preventDefault();
    if (this.props.onImageSubmitted) this.props.onImageSubmitted(this.uploadInput.files[0])
  }

  render() {
    return (
      <Form className="p-2" onSubmit={this.onPictureSubmit.bind(this)}>
        <Form.Control type="file" id="fileInput" ref={(ref) => {this.uploadInput = ref}}></Form.Control>
        <Button className="mt-2" type="submit" >Upload</Button>
      </Form>);
    }
}