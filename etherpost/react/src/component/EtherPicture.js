import React, { Component } from 'react';
import '../App.css';
import PropTypes from 'prop-types';
import IpfsTools from './ipfs_utils';
import EmbarkJS from '../embarkArtifacts/embarkjs';
import IPFS from 'ipfs';
/**
 * props:
 * - ipfs
 * - imageHash (in hex32)
 */
export default class EtherPicture extends Component {
  static propTypes = {
    ipfs: PropTypes.object,
    imageHash: PropTypes.string,
  };
  ipfs = new IPFS();
  constructor(props) {
    super(props)
    this.state = {
      imageData: null,
      ipfsHash: null
    }
  }

  async componentWillMount() {
    if (this.props.imageHash) {
      console.log(this.props.imageHash)
      const result = await this.readFile(IpfsTools.hex32ToHash(this.props.imageHash));
      this.setState({
        imageData: result.imageData,
        ipfsHash: result.hash
      });  
    }
  }

  /**
   * There is a bug with Embark.Storage.get() API where it forces the content to be string on the way out.
   * So the image is alrady stored as base64 string.
   */
  async readFile(hash) {
    if (hash === null) return;
    console.log(`EtherPicture.readFile(${hash}) ...`);
    const buffer = await EmbarkJS.Storage.get(hash); // Output is forced to be a string
    console.log(`EtherPicture.readFile(${hash}): read ${buffer.length} bytes`);
    return {
      imageData: 'data:;base64,' + buffer,
      hash: hash
    };
  }

  render() {
    if (!this.state.imageData) return "No image";
    return (
      <img src={this.state.imageData} alt={this.state.ipfsHash} title={this.state.ipfsHash} style={{ ...this.props.style }}/>);
    }
}
