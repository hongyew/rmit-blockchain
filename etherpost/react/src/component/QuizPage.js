import React, { Component } from 'react';
import '../App.css';
import { Container, Row, Col, Button, Form, Toast, Navbar, ListGroup, NavbarBrand, Badge } from 'react-bootstrap';
import IpfsTools from './ipfs_utils';
import EmbarkJS from '../embarkArtifacts/embarkjs';
import Etherpost from '../embarkArtifacts/contracts/Etherpost';
import EtherPicture from './EtherPicture';
/**
 * The incoming url parameters are
 * - account:   this.props.match.params.account
 * - imageHash: this.props.match.params.imageHash
 * 
 */
export default class QuizPage extends Component {
  ipfs = null;
  commentInput = {}

  constructor(props) {
    super(props)
    console.log('QuizPage', this.props);
    this.state = {
      showToast: false,
      toastMassage: null,
      toastTitle: null,
      comments: [],
      question: null,
      answer: null,
      answerIpfs: null,
      winner: null,
      claps: 0
    }
  }

  async componentDidMount() {
    // Expects imageHash to be in the URL
    console.log(this.props.match.params.imageHash)
    if (this.props.match.params.imageHash) {
      await this.loadQuiz();
      await this.loadComment();
      await this.loadResult()

      // Register events
      Etherpost.events.LogAnswer(this.onAnswerEvent.bind(this));
      Etherpost.events.LogClap(this.onClapEvent.bind(this));
    }
  }

  componentWillUnmount() {
    Etherpost.events.LogAnswer(null);
    Etherpost.events.LogClap(null);
  }

  async loadQuiz() {
    console.log('loadQuiz():')
    const hex32 = await Etherpost.methods.getQuiz(this.props.match.params.imageHash).call();
    const ipfsHash = IpfsTools.hex32ToHash(hex32)
    const quizQuestion = await EmbarkJS.Storage.get(ipfsHash);

    if (quizQuestion) {
      this.setState({ question: quizQuestion })
    }
  }

  async loadResult() {
    console.log('loadResult():')
    const winner = await Etherpost.methods.getWinner(this.props.match.params.imageHash).call()
    // If we have the answer and the winner, declare the game over
    if (winner > 0) {
      // revealAnswer() could throw only if there is no winner.
      const answerIpfsHex32 = await Etherpost.methods.revealAnswer(this.props.match.params.imageHash).call()
      this.gameOver(IpfsTools.hex32ToHash(answerIpfsHex32), winner)

      // Update claps count
      const countAsString = await Etherpost.methods.getClapCount(this.props.match.params.imageHash).call()  // returns string
      this.setState({ claps: parseInt(countAsString) })
    }
  }

  async loadComment() {
    console.log('loadComment():')
    const commentsHex = await Etherpost.methods.getComments(this.props.match.params.imageHash).call();
    const comments = []

    for (let i in commentsHex) {
      const ipfsHash = IpfsTools.hex32ToHash(commentsHex[i])
      comments.push({
        text: await EmbarkJS.Storage.get(ipfsHash),
        hash: ipfsHash,
        key: i
      });
    }
    
    if (comments.length > 0) {
      this.setState({ comments: comments })
    }
  }

  toast(title, message) {
    this.setState({
      showToast: true,
      toastMassage: message,
      toastTitle: title
    })
  }

  async gameOver(answerIpfs, winner) {
    this.setState({
      answer: await EmbarkJS.Storage.get(answerIpfs),
      answerIpfs: answerIpfs,
      winner: winner
    })
  }

  //------------------------------------------------------------------------------------------------------------------------
  // UI Event Handlers
  //------------------------------------------------------------------------------------------------------------------------

  async onSubmitAnswer(e) {
    e.preventDefault();
    const text = this.answerInput.value
    if (!text || text.length === 0) return;
    console.log('onSubmitAnswer()', text)
  
    // Reset any toast
    this.setState({ showToast: false });
    try {
      // Store comment IPFS
      const commentHash = await EmbarkJS.Storage.saveText(text)
      console.log(`Comment in IPFS ${commentHash}`);

      // Upload comment hash to contract
      const hex32 = IpfsTools.hashToHex32(commentHash); // convert from IPFS hash to byte32 hex

      // Need to use .send() to trigger event
      const submitingAnswer = text.toUpperCase();
      const answerContract = Etherpost.methods.submitAnswer(this.props.match.params.imageHash, submitingAnswer, hex32)
      const estimatedGas = Math.round((await answerContract.estimateGas({from: this.props.account })) * 1.1);

      console.log('onSubmitAnswer -> Etherpost.submitAnswer(): ', submitingAnswer, estimatedGas)
      const tx = await answerContract.send({ from: this.props.account, gas: estimatedGas })
      console.log('onSubmitAnswer -> Etherpost.submitAnswer() completed: ', tx)
      if (!tx.events.LogAnswer.returnValues.result) {
        this.toast('Try again', text + " wasn't the right answer")
      }
    }
    // This will catch revert error from Smart Contract
    // eg. the game host cannot answer comment
    catch(e) {
      console.error(e)
      this.toast('Error', e.message)
    }
  }

  async onClap() {
    await Etherpost.methods.clap(this.props.match.params.imageHash).send({ from: this.props.account })
  }

  /**
   * Handles LogAnswer event from contract:
   * LogAnswer(sender, bytes32 imageHash, bool result, bytes32 ipfsAnswer, uint timestamp)
   * 
   * If the hash is for this image AND somebody won, game over. 
   */
  async onAnswerEvent(err, event) {
    if (event.returnValues.imageHash === this.props.match.params.imageHash) {
      console.log("onAnswerEvent():", event)

      // Add answer to comment state
      const comments = this.state.comments;
      const answerIpfs = IpfsTools.hex32ToHash(event.returnValues.answerIpfs)
      comments.push({
          text: await EmbarkJS.Storage.get(answerIpfs),
          hash: answerIpfs,
          key: comments.length
      })
      console.log(comments);
      this.setState({ comments: comments })

      // If there is a correct answer
      if (event.returnValues.result) {
        this.gameOver(answerIpfs, event.returnValues.sender)
      }
    }
  }

  /**
   * event LogClap(address clapper, bytes32 ipfsHash, uint count);
   */
  async onClapEvent(err, event) {
    if (event.returnValues.ipfsHash === this.props.match.params.imageHash) {
      console.log("onClapEvent():", event)
      this.setState({ claps: parseInt(event.returnValues.count) })
    }
  }

  //------------------------------------------------------------------------------------------------------------------------
  // Rendering logic
  //------------------------------------------------------------------------------------------------------------------------
  renderComments() {
    console.log('renderComments():', this.state.comments);
    if (!this.state.comments) return (<center style={{ paddingTop: 16 }} >No answers</center>)
    const item = (comment) => 
    <ListGroup.Item key={comment.key} style={{ paddingTop: 8, paddingBottom: 8  }}>
       <span role='img' aria-label='face' style={{ paddingRight: 12 }}>🙄&nbsp;</span>
       <span >{comment.text}</span>
    </ListGroup.Item>
    return (
      <ListGroup >
      {this.state.comments.map((comment) => item(comment))}
      </ListGroup>
    )
  }

  renderWinner() {
    if (!this.state.winner) return ""
    return (
      <Container>
        <p style={{ textAlign: 'center' }}>Player <code>{this.state.winner}</code> won!</p> 
        <p style={{ textAlign: 'center' }}>The answer is '{this.state.answer}'</p>
      </Container>)
  }

  renderClapMessage() {
    return (
      <Badge style={{ padding: 12 }} variant="light">You received {this.state.claps} 👏</Badge>
    )
  }

  renderClapButton() {
    return (
      <Button variant="outline-info" onClick={this.onClap.bind(this)}>
        <span role='img' aria-label='clap'>👏</span> Clap! 
        <Badge style={{ marginLeft: 8 }} variant="light">{this.state.claps}</Badge>
      </Button>
    )
  }

  renderFooter() {
    if (!this.state.winner) return ""
    return (
      <Navbar fixed="bottom" variant="dark" bg="dark">
        <Container className="d-flex justify-content-center">
          <NavbarBrand >Game Over</NavbarBrand>
        </Container>
        {(this.state.winner.toLowerCase() === this.props.account)? this.renderClapMessage(): this.renderClapButton()}
      </Navbar>)
  }

  renderAnswerInput() {
    if (this.state.winner) return ""

    // If the current user is the host
    if (this.props.account === this.props.match.params.account) {
      return (<center>You are the host of this game. Send this page URL to your friend to play the game.</center>)
    }

    // Not the host, user can play the game
    return (
      <Form className="input-sm" onSubmit={this.onSubmitAnswer.bind(this)}>  
      <Form.Group as={Row} controlId="formAnswer">
        <Col sm="9"><Form.Control type="text" ref={(r) => this.answerInput = r} placeholder="Your answer" /></Col>
        <Col sm="3"><Button type="submit" >Answer</Button></Col>
      </Form.Group>
    </Form>)
  }

  renderToast() {
    return(
      <Toast show={this.state.showToast} onClose={()=> this.setState({ showToast: false })} style={{ position: 'absolute', top: 140, left: 140}}>
        <Toast.Header>
          <strong className="mr-auto">{this.state.toastTitle}</strong>
        </Toast.Header>
        <Toast.Body>{this.state.toastMassage}</Toast.Body>
      </Toast>
    )
  }

  render() {
    return (
      <Container>
        <h4 style={{ textAlign: 'center' }}>{this.state.question}</h4>
        {this.renderWinner()}

        <Row style={{ marginTop: '1rem', marginBottom: '1rem' }}>
          <Col >
            <EtherPicture ipfs={this.ipfs} imageHash={this.props.match.params.imageHash} style={{  marginRight: 'auto', marginLeft: 'auto', display: 'block', marginBottom: '1rem' }}/>          
          </Col>
          <Col>
            {this.renderComments()}
          </Col>
        </Row>
   
        {this.renderAnswerInput()}
        {this.renderToast()}
        {this.renderFooter()}
      </Container>) 
  }
}