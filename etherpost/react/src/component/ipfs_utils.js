import bs58 from 'bs58'

export default class IpfsTools {

    /**
     * Convert IPFS hash to hex byte32 for efficient Solidity contract hash storage
     * 
     * https://learn.rmitonline.edu.au/courses/course-v1:RMIT+BLC202+2019_JUN_B/courseware/7c5fbbd51bb14156be3b6f831f459922/3711d0ff7e524db68f3c469852d2d67f/?child=first
     * @param {*} ipfsHash
     * @return byte32 in hex 
     */
    static hashToHex32(ipfsHash) {
        let decoded = bs58.decode(ipfsHash)
        let sliced = decoded.slice(2)
        let hex = sliced.toString('hex')
        let result = '0x' + hex
        return result;
    }

    /**
     * Convert byte32 hex hash notation back to standard IPFS hash notation
     * 
     * https://learn.rmitonline.edu.au/courses/course-v1:RMIT+BLC202+2019_JUN_B/courseware/7c5fbbd51bb14156be3b6f831f459922/3711d0ff7e524db68f3c469852d2d67f/?child=first
     * @param {*} bytes32Hex 
     */
    static hex32ToHash(bytes32Hex) {
        // Add default IPFS values for first 2 bytes: function: 0x12=sha2, size: 0x20=256 bits
        // Cut off leading "0x"
        let hex = "1220" + bytes32Hex.slice(2)
        let hashBytes = Buffer.from(hex, 'hex')
        let str = bs58.encode(hashBytes)
        return str
      }
}
